package pl.edu.pwsztar

import spock.lang.Specification

class SumAccountsBalanceSpec extends Specification {

    def "should sum balances of all accounts"() {

        given: "initial data"
            def deposits = [123, 321, 213, 312]
            def bank = new Bank()

            def accNumber1 = bank.createAccount()
            def accNumber2 = bank.createAccount()
            def accNumber3 = bank.createAccount()

            bank.deposit(accNumber1, deposits[0])
            bank.deposit(accNumber2, deposits[1])
            bank.deposit(accNumber3, deposits[2])
            bank.deposit(accNumber2, deposits[3])
        when: "balance of all accounts is summed"
            def allFunds = bank.sumAccountsBalance()
        then: "check if sum is correct"
            deposits.sum() == allFunds
    }

}

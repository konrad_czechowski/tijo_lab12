package pl.edu.pwsztar

import spock.lang.Specification

class AccountBalanceSpec extends Specification {

    def "should return balance of existing account"() {

        given: "initial data"
            def depositAmount = 687
            def bank = new Bank()
            def number = bank.createAccount()
            bank.deposit(number, depositAmount)
        when: "checking account balance"
            def balance = bank.accountBalance(number)
        then: "check if balance is equal to deposited money"
            balance == depositAmount
    }

    def "should return ACCOUNT_NOT_EXISTS if trying to get balance of non existing account"() {

        given: "initial data"
            def bank = new Bank()
        when: "checking account balance"
            def balance = bank.accountBalance(bank.accountNumber+100)
        then: "check if balance is equal to deposited money"
            balance == BankOperation.ACCOUNT_NOT_EXISTS
    }
}

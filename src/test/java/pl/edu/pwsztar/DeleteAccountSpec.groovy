package pl.edu.pwsztar

import spock.lang.Specification

class DeleteAccountSpec extends Specification {

    def "should delete account without funds"() {

        given: "initial data"
            def bank = new Bank()
            def number = bank.createAccount()
        when: "the account is deleted"
            def moneyLeft = bank.deleteAccount(number)
        then: "check money left"
            moneyLeft == 0
    }

    def "should return ACCOUNT_NOT_EXISTS when deleted account does not exist"() {

        given: "initial data"
            def bank = new Bank()
        when: "the account is deleted"
            def moneyLeft = bank.deleteAccount(bank.accountNumber+100)
        then: "check money left"
            moneyLeft == BankOperation.ACCOUNT_NOT_EXISTS
    }

    def "should delete account with funds"() {

        given: "initial data"
            def moneyDeposited = 100
            def bank = new Bank()
            def number = bank.createAccount()
            bank.deposit(number, moneyDeposited)
        when: "the account is deleted"
            def moneyLeft = bank.deleteAccount(number)
        then: "check money left"
            moneyLeft == moneyDeposited
    }

}

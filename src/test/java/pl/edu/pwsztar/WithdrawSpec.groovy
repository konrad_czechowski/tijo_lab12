package pl.edu.pwsztar

import spock.lang.Specification

class WithdrawSpec extends Specification{

    def "should withdraw money from existing account"() {

        given: "initial data"
            def amount = 387
            def bank = new Bank()
            def number = bank.createAccount()
            bank.deposit(number, amount)
        when: "money is withdrawn from the account"
            def returnValue = bank.withdraw(number, amount)
        then: "check if withdraw is successful"
            returnValue
    }

    def "should not withdraw money from non existing account"() {

        given: "initial data"
            def amount = 3452
            def bank = new Bank()
        when: "money is withdrawn from the account"
            def returnValue = bank.withdraw(bank.accountNumber+100, amount)
        then: "check if withdraw is successful"
            !returnValue
    }

    def "should not withdraw money from account with insufficient balance"() {

        given: "initial data"
            def amount = 123
            def bank = new Bank()
            def number = bank.createAccount()
            bank.deposit(number, amount)
        when: "money is withdrawn from the account"
            def returnValue = bank.withdraw(number, amount+1)
        then: "check if withdraw is successful"
            !returnValue
    }
}

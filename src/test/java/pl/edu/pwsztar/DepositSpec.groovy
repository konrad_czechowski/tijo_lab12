package pl.edu.pwsztar

import spock.lang.Specification

class DepositSpec extends Specification {

    def "should deposit money on existing account"() {

        given: "initial data"
            def amount = 387
            def bank = new Bank()
            def number = bank.createAccount()
        when: "money is deposited on the account"
            def returnValue = bank.deposit(number, amount)
        then: "deposit is successful"
            returnValue
    }

    def "should not deposit money on non existing account"() {

        given: "initial data"
            def amount = 3452
            def bank = new Bank()
        when: "money is deposited on the account"
            def returnValue = bank.deposit(bank.accountNumber+100, amount)
        then: "deposit is successful"
            !returnValue
    }
}

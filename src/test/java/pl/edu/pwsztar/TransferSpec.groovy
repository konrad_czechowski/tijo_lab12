package pl.edu.pwsztar

import spock.lang.Specification

class TransferSpec extends Specification {

    def "should transfer money if there is sufficient balance"() {

        given: "initial data"
            def bank = new Bank()
            def acc1 = bank.createAccount()
            def acc2 = bank.createAccount()
            bank.deposit(acc1, 150)
        when: "transferring money between two accounts"
            def depositSuccessful = bank.transfer(acc1, acc2, 100)
        then: "check if transfer was successful"
            depositSuccessful
    }

    def "should not transfer money if there is not sufficient balance"() {

        given: "initial data"
            def bank = new Bank()
            def acc1 = bank.createAccount()
            def acc2 = bank.createAccount()
            bank.deposit(acc1, 356)
        when: "transferring money between two accounts"
            def depositSuccessful = bank.transfer(acc1, acc2, 400)
        then: "check if transfer was successful"
            !depositSuccessful
    }

    def "should not transfer money when recipient account does not exist"() {

        given: "initial data"
            def bank = new Bank()
            def acc1 = bank.createAccount()
            bank.deposit(acc1, 456)
        when: "transferring money between two accounts"
            def depositSuccessful = bank.transfer(acc1, bank.accountNumber+100, 240)
        then: "check if transfer was successful"
            !depositSuccessful
    }

    def "should not transfer money when sender account does not exist"() {

        given: "initial data"
            def bank = new Bank()
            def acc1 = bank.createAccount()
            bank.deposit(acc1, 456)
        when: "transferring money between two accounts"
            def depositSuccessful = bank.transfer(bank.accountNumber+100, acc1, 120)
        then: "check if transfer was successful"
            !depositSuccessful
    }
}

package pl.edu.pwsztar;

import java.util.HashMap;

public class AccountsDB {

    private HashMap<Integer, Account> accountList = new HashMap<>();

    public int createAccount(int accountNumber, int balance) {
        accountList.put(accountNumber, new Account(accountNumber, balance));
        return accountNumber;
    }

    public int deleteAccount(int number) {

        try {
            int balance = accountList.get(number).getBalance();
            accountList.remove(number);
            return balance;
        } catch (NullPointerException e) {
            System.err.println("Account does not exist");
        }
        return BankOperation.ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int number, int amount) {
        try {
            accountList.get(number).deposit(amount);
            return true;
        } catch (NullPointerException e) {
            System.err.println("Account does not exist");
        }
        return false;
    }

    public boolean withdraw(int number, int amount) {
        try {
            int balance = accountList.get(number).getBalance();
            if(balance >= amount) {
                accountList.get(number).withdraw(amount);
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            System.err.println("Account does not exist");
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        try {
            int balanceFrom = accountList.get(fromAccount).getBalance();
            if(balanceFrom >= amount) {
                accountList.get(fromAccount).withdraw(amount);
                accountList.get(toAccount).deposit(amount);
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            System.err.println("Account does not exist");
        }
        return false;
    }

    public int accountBalance(int number) {
        try {
            return accountList.get(number).getBalance();
        } catch (NullPointerException e) {
            System.err.println("Account does not exist");
        }
        return BankOperation.ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        return accountList.values().stream().mapToInt(Account::getBalance).sum();
    }
}

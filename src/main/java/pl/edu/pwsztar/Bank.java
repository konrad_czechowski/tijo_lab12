package pl.edu.pwsztar;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private static int accountNumber = 0;
    private AccountsDB accountsDB;

    public Bank() {
        this.accountsDB = new AccountsDB();
    }

    public int createAccount() {
        return accountsDB.createAccount(++accountNumber, 0);
    }

    public int deleteAccount(int accountNumber) {
        return accountsDB.deleteAccount(accountNumber);
    }

    public boolean deposit(int accountNumber, int amount) {
        return accountsDB.deposit(accountNumber, amount);
    }

    public boolean withdraw(int accountNumber, int amount) {
        return accountsDB.withdraw(accountNumber, amount);
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        return accountsDB.transfer(fromAccount, toAccount, amount);
    }

    public int accountBalance(int accountNumber) {
        return accountsDB.accountBalance(accountNumber);
    }

    public int sumAccountsBalance() {
        return accountsDB.sumAccountsBalance();
    }
}
